use std::sync::Arc;

use cpal::InputCallbackInfo;
use rustfft::Fft;
use rustfft::{num_complex::Complex, FftPlanner};

use cpal::traits::{DeviceTrait, HostTrait, StreamTrait};

fn main() {
    let host = cpal::default_host();

    // Set up the input device and stream with the default input config.
    let device = host.default_input_device().unwrap();

    println!("Input device: {}", device.name().unwrap());

    let config = device.default_input_config().unwrap();
    println!("Default input config: {:?}", config);

    // A flag to indicate that recording is in progress.
    println!("Begin recording...");

    let err_fn = move |err| {
        eprintln!("an error occurred on stream: {}", err);
    };

    let mut planner = FftPlanner::<f32>::new();
    let fft = planner.plan_fft_forward(6340);

    let stream = match config.sample_format() {
        cpal::SampleFormat::F32 => device
            .build_input_stream(
                &config.into(),
                move |data, info| write_input_data(data, info, fft.clone()),
                err_fn,
                None,
            )
            .unwrap(),
        sample_format => {
            eprintln!("Unsupported sample format '{sample_format}'");
            return;
        }
    };

    stream.play().unwrap();

    // Let recording go for roughly three seconds.
    std::thread::sleep(std::time::Duration::from_secs(3));
    drop(stream);
}

fn write_input_data(input: &[f32], info: &InputCallbackInfo, fft: Arc<dyn Fft<f32>>) {
    println!("==================");
    // println!("Test: {}", input.len());
    // dbg!(input);
    // println!("info: {:?}", info);

    let mut buffer: Vec<Complex<f32>> = input
        .iter()
        .map(|el| Complex {
            re: *el,
            im: 0.0f32,
        })
        .collect();

    buffer.resize(
        6340,
        Complex {
            re: 0.0f32,
            im: 0.0f32,
        },
    );
    fft.process(&mut buffer);
    let max_peak = buffer
        .iter()
        .take(input.len() / 2)
        .enumerate()
        .max_by_key(|&(_, freq)| freq.norm() as u32)
        .unwrap();
    let freq_step = 1.0f32 / 6340.0f32 * 44100.0f32;
    let freq = freq_step * max_peak.0 as f32;
    println!("Freq: {:?}Hz Max peak: {:?}", freq, max_peak.1);
    //dbg!(buffer);
}

// InputCallbackInfo {
//     timestamp: InputStreamTimestamp {
//         callback: StreamInstant {
//             secs: 1,
//             nanos: 518477108
//         },
//         capture: StreamInstant {
//             secs: 1,
//             nanos: 454282097
//         }
//     }
// }
